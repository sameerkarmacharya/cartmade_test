<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Modals\Category;

Route::get('/', function () {


    return view('index');
});
//
Route::get('/content', function () {


    return view('content');
});


Route::get('/about/show/id', function () {

	$categories = Category::with('product','products')->get();

    return view('about', compact('categories'));
})->name('about');
//
//
//Route::get('category', 'CategoryController@index')->name('category.index');
//Route::get('category/create', 'CategoryController@create')->name('category.create');
//Route::delete('category/samir/{id}/{asd}', 'CategoryController@create')->name('category.create');
//
//
//Route::get('product', 'Admin\ProductController@index')->name('product.index');

Route::any('handleorder', 'ContactController@handleOrder');

//Route::resource('contact', 'ContactController');





<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Modals\Category;
use App\Modals\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $viewType = 'asc';
        $contacts = Contact::orderBy('name', $viewType)->paginate(2);

        return view('contact-list', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Category::query()->pluck('name', 'id');
        Session::forget('success');
        return view('contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        $insertData = $request->except('_token');
        if (!empty($insertData['image'])) {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
            $insertData['image'] = $imageName;
        }
        Contact::create($insertData);
        Session::put('success', 'Created');


        return redirect()->route('contact.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public
    function show(
        $id
    ) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit(
        $id
    ) {
        $contact = Contact::where('id', $id)->first();
        if (!$contact) {
            abort(404);
        }
        return view('contact-edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(
        ContactRequest $request,
        $id
    ) {
        $contact = Contact::findOrFail($id);

        $contact->update($request->except('_token'));

        return redirect()->route('contact.index')->with('success', 'Updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(
        $id
    ) {
        Contact::find($id)->delete();
        return redirect()->route('contact.index')->with('success', 'deleted.');

    }


    /**
     */
    public function handleOrder()
    {

        Log::info('handle order called.');

        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        Log::info('header '.$hmac_header);
        $data        = file_get_contents('php://input');


//        Log::info($data);
        $verified    = $this->verify_webhook($data, $hmac_header);
        if ($verified) {
            Log::info(json_encode($data));
            Log::info('Verified.');

        }

        error_log('Webhook verified: '.var_export($verified, true)); //c

    }

    private function verify_webhook($data, $hmac_header)
    {
//        Log::info('sent to verify');

        $calculated_hmac = base64_encode(hash_hmac('sha256', $data,
            '9d0ac12cf5e17d3bd07742f5faf4380ba742f5c7dc598c7d173d6763601e0110', true));
        Log::info('cal '.$calculated_hmac);
        if ($hmac_header == $calculated_hmac) {
            Log::info('success');
            return true;

        } else {
            Log::info('error');

            return false;
        }
    }

}

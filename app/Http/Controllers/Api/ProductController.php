<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modals\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{


    public function update()
    {
        Log::info('handle order called.');

        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        Log::info('header '.$hmac_header);
        $data      = file_get_contents('php://input');




//        Log::info($data);
        $verified = $this->verify_webhook($data, $hmac_header);
        if ($verified) {
            Log::info('verified');
//            Log::info(json_encode($data)->id);

            $orderData = json_decode($data);
//
  $lineItems = $orderData->line_items;
            Log::info(  $lineItems);

            foreach ($lineItems as $od) {

                $order                          = new Order();

                $order->id                      = $od->id;
                $order->quantity                = $od->quantity;
                $order->variant_id              = $od->variant_id;
                $order->product_id              = $od->product_id;
                $order->price                   = $od->price;
                $order->shop_money_amount       = $od->discounted_price_set->shop_money->amount;
                $order->shop_money_currency_code = $od->discounted_price_set->shop_money->currency_code;
                $order->save();
                Log::info('Order created.');

            }

            Log::info('Verified.');

        }

        error_log('Webhook verified: '.var_export($verified, true)); //c

    }

    private function verify_webhook($data, $hmac_header)
    {
//        Log::info('sent to verify');

        $calculated_hmac = base64_encode(hash_hmac('sha256', $data,
            '9d0ac12cf5e17d3bd07742f5faf4380ba742f5c7dc598c7d173d6763601e0110', true));
        Log::info('cal '.$calculated_hmac);
        if ($hmac_header == $calculated_hmac) {
            Log::info('success');
            return true;

        } else {
            Log::info('error');

            return false;
        }
    }
}

<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{


    protected $fillable = [
        'id',
        'quantity',
        'variant_id',
        'product_id',
        'price',
        'shop_money_amount',
        'shop_money_currency_code'
    ];
}

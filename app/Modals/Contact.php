<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

protected $fillable =['name','email','phone','message','image'];
}

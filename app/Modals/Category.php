<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['name', 'slug', 'description', 'is_active', 'category_order', 'image'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */

    public function product()
    {
        return $this->hasOne(Product::class);
    }

    /**
     *
     */
    public function products()
    {

        return $this->hasMany(Product::class);
    }

}

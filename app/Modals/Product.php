<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'category_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {

        return $this->belongsTo(Category::class);

    }


    /**
     *
     */
    public function categories(){
        return $this->belongsToMany(Category::class,'product_category');
    }
}


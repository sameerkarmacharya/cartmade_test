<?php

use App\Modals\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name'           => 'Clothes',
                'slug'           => 'clothes',
                'is_active'      => 1,
                'description'    => 'this is clothes',
                'category_order' => 1
            ],
            [
                'name'           => 'Shoes',
                'slug'           => 'shoes',
                'is_active'      => 1,
                'description'    => 'this is shoes',
                'category_order' => 1
            ],
            [
                'name'           => 'Kitchen',
                'slug'           => 'kitchen',
                'is_active'      => 1,
                'description'    => 'this is kitchen',
                'category_order' => 1
            ],
            [
                'name'           => 'Sports',
                'slug'           => 'sports',
                'is_active'      => 1,
                'description'    => 'Laravel should always be served out of the root of the',
                'category_order' => 1
            ]
        ];

        foreach ($categories as $category) {
            Category::updateOrCreate(['slug' => $category['slug']], $category);
//            Category::create($category);

            //            $c                 = new Category();
//            $c->name           = $category['name'];
//            $c->slug           = $category['slug'];
//            $c->is_active      = $category['is_active'];
//            $c->description    = $category['description'];
//            $c->category_order = $category['category_order'];
//            $c->save();
        }
    }
}


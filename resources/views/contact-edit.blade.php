@extends('index')
@section('content')
    <div id="wrapper">
        <div id="featured-wrapper">
            <form action="{{route('contact.update', $contact->id)}}" method="post">
                @csrf
                <label for="">Name</label>
                <input type="text" name="name" value="{{ old('name',$contact->name)}}" />
{!! $errors->first('name') !!}
                <br/>

<input type="hidden" name="_method" value="put">
                <label for="">Email</label>
                <input type="text" name="email" value="{{old('email',$contact->email)}}"/>
{!! $errors->first('email') !!}
                
                <br/>
                <label for="">Phone</label>
                <input type="text" name="phone" value="{{ old('phone',$contact->phone)}}"/>
{!! $errors->first('phone') !!}

                <br/>
                <label for="">Message</label>
                <textarea name="message" id="" cols="30" rows="10"> {{ old('message',$contact->message)}}</textarea>
{!! $errors->first('message') !!}

                <br/>
                <button type="submit"> Submit</button>
            </form>
        </div>
    </div>
@endsection

@extends('index')
@section('content')
    <div id="wrapper">
        <div id="featured-wrapper">
            <form action="{{route('contact.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Name</label>
                <input type="text" name="name"/>
                {!! $errors->first('name') !!}
                <br/>


                <label for="">Email</label>
                <input type="text" name="email"/>
                {!! $errors->first('email') !!}

                <br/>
                <label for="">Phone</label>
                <input type="text" name="phone" value=""/>
                {!! $errors->first('phone') !!}

                <br/>
                <label for="">Image</label>
                <input type="file" name="image"/>
                {!! $errors->first('image') !!}

                <br/>
                <label for="">Message</label>
                <textarea name="message" id="" cols="30" rows="10"></textarea>
                {!! $errors->first('message') !!}

                <br/>
                <button type="submit"> Submit</button>
            </form>
        </div>
</div>
@endsection

product ->category-> name

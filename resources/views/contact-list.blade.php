@extends('index')
@section('content')
    <div id="wrapper">
        <div id="featured-wrapper">

            @if(Session::has('success'))
                {{ Session::get('success')}}
            @endif


            <table>
                <thead>
                <tr>

                    <th>ID</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>

                </thead>
                @forelse($contacts as $contact)
                    <tr>
                        <td>{{$contact->id }}</td>
                        <td><img src="{{ asset('images/'.$contact->image) }}" alt="" width="100px" height="100px"/></td>
                        <td>{{$contact->name }}</td>
                        <td>{{$contact->email }}</td>
                        <td>{{$contact->phone }}</td>
                        <td>{{$contact->message }}</td>
                        <td>
                            <a href="{{route('contact.edit',$contact->id)}}">Edit</a>
                            <form method="post" action="{{route('contact.destroy', $contact->id)}}">
                                @csrf
                                <input type="hidden" name="_method" value="delete">
                                <button type="submit">Delete</button>
                            </form>

                        </td>

                    </tr>
                @empty
                    <tr>

                        <td colspan="6">no data</td>
                    </tr>
                @endforelse
                <tbody>

                </tbody>
            </table>
            {!! $contacts->links()!!}
        </div>
    </div>
    }
    }
@endsection
